import React, { Component, Fragment } from "react";
import {
  Typography,
  IconButton,
  Icon,
  withStyles,
  InputAdornment,
  Grid,
  TextField
} from "@material-ui/core";
import classNames from "classnames";
import PropTypes from "prop-types";
import TableComponent from "./tableComponent";
import "../index.css";

const styles = {
  searchBox: {
    width: "50%"
  },
  icon: {
    fontSize: "18px",
    color: "#a2a2a2"
  }
};

class SearchComponent extends Component {
  state = {
    query: "", // this is the search text entered
    allUserData: [], // this contains all the user records
    filteredUserData: [] // this contains the filtered records and used to display
  };

  componentDidMount() {
    const userData = require("../data/users.json");
    this.setState({
      query: "",
      allUserData: userData,
      filteredUserData: []
    });
  }

  handleSearchTextChange = event => {
    const queryText = event.target.value;
    // if the search text is * then show all the valeus.
    if (queryText === "*") {
      this.setState({
        query: queryText,
        filteredUserData: this.state.allUserData
      });
      return;
    }
    // check if there is user data and then
    // check if the query text has changed. Do nothing if the
    // query text is same as that of the previous one.
    if (
      this.state.allUserData.length &&
      queryText.toLowerCase() !== this.state.query.toUpperCase()
    ) {
      this.search(queryText);
    }
  };

  search(queryText) {
    // set the state of the query
    this.setState({ query: queryText });
    const allUserData = this.state.allUserData;
    const newData = allUserData.filter(data => {
      // gotta search in all the properties of the object.
      // So, create a list of all the values of the object
      const values = Object.values(data).map(v => v.toLowerCase());
      // check if the entered search query is in the list of values
      return values.includes(queryText.trim().toLowerCase());
    });
    // set the state of the filteredUserData to the newly 
    // generated array
    this.setState({ filteredUserData: newData });
  }

  render() {
    const { classes } = this.props;

    return (
      <Fragment>
        <div className="spacer" />
        <Typography variant="h4">Search Customer Base</Typography>
        <div className="spacer" />
        <Grid container>
          <Grid item sm={12}>
            <TextField
              className={classes.searchBox}
              id="input-with-icon-textfield"
              placeholder="Start typing to search..."
              value={this.state.query}
              onChange={this.handleSearchTextChange}
              InputProps={{
                startAdornment: (
                  <InputAdornment position="start">
                    <Icon
                      className={classNames("fa fa-search", classes.icon)}
                    />
                  </InputAdornment>
                ),
                endAdornment: this.state.query.length ? (
                  <InputAdornment position="start">
                    <IconButton
                      className={classNames("fa fa-close", classes.icon)}
                      onClick={() => {
                        this.setState({ query: "", filteredUserData: [] });
                      }}
                    />
                  </InputAdornment>
                ) : (
                  ""
                )
              }}
            />
          </Grid>
        </Grid>
        <div className="spacer" />
        <TableComponent {...this.state} />
      </Fragment>
    );
  }
}

SearchComponent.propTypes = {
  classes: PropTypes.object.isRequired
};
export default withStyles(styles)(SearchComponent);
