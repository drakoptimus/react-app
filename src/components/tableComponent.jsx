import React, { Component, Fragment } from "react";
import {
  Grid,
  withStyles,
  Paper,
  Table,
  TableBody,
  TableCell,
  TableRow,
  TableHead,
  Typography,
  Icon
} from "@material-ui/core";
import PropTypes from "prop-types";
import classNames from "classnames";
import classnames from "classnames";

const styles = {
  largeMarginTop: {
    marginTop: "20px"
  },
  resultsContainer: {
    minHeight: "10em"
  },
  iconFont: {
    fontSize: "50px"
  }
};

// This is a reusable component. All you need to do is to pass array of objects
// this handles all the cases gracefully.
class TableComponent extends Component {
  render() {
    const { classes } = this.props;
    return (
      <div className={classNames(classes.largeMarginTop)}>
        <Grid container justify="center">
          <Grid item sm={11}>
            <Paper className={classNames(classes.resultsContainer)}>
              <DisplayResults {...this.props} />
            </Paper>
          </Grid>
        </Grid>
      </div>
    );
  }
}

function DisplayResults(props) {
  // when there is data available to display
  // render the table
  if (props.filteredUserData.length) {
    return <UserDataTable {...props} />;
  }

  // if there is query text and no results to be displayed
  // then it means that there are no results
  if (props.filteredUserData.length === 0 && props.query.length) {
    return <NoSearchResults {...props} />;
  }

  // this is rendered when nothing is entered.
  return <SearchPlaceHolder {...props} />;
}

function SearchPlaceHolder(props) {
  return (
    <Fragment>
      <div className="spacer" />
      <Icon className={classnames("fa fa-grav", props.classes.iconFont)} />
      <Typography variant="title">
        Start Searching.
        <br />
        Search '*' to display customers.
      </Typography>
    </Fragment>
  );
}

function NoSearchResults(props) {
  return (
    <Fragment>
      <div className="spacer" />
      <Icon className={classnames("fa fa-frown-o", props.classes.iconFont)} />
      <Typography variant="title">
        Sorry! No customers found.
        <br />
        Search '*' to display all customers.
      </Typography>
    </Fragment>
  );
}

function UserDataTable(props) {
  return (
    <Fragment>
      <Table>
        <TableHead>
          <TableRow>
            <TableCell>UserName</TableCell>
            <TableCell>FirstName</TableCell>
            <TableCell>LastName</TableCell>
            <TableCell>Age</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {props.filteredUserData.map((data, index) => {
            return (
              <TableRow key={index}>
                <TableCell>{data.userName}</TableCell>
                <TableCell>{data.firstName}</TableCell>
                <TableCell>{data.lastName}</TableCell>
                <TableCell>{data.age}</TableCell>
              </TableRow>
            );
          })}
        </TableBody>
      </Table>
    </Fragment>
  );
}

TableComponent.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(TableComponent);
