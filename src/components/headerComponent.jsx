import React, { Component } from "react";
import {
  AppBar,
  Icon,
  Typography,
  Toolbar,
  IconButton,
  withStyles
} from "@material-ui/core";
import classNames from "classnames";
import PropTypes from "prop-types";
import classnames from "classnames";

const styles = {
  // This group of buttons will be aligned to the right
  rightToolbar: {
    marginLeft: "auto",
    marginRight: -12
  },
  colorWhite: {
    color: "#fff"
  },
  bgBlue: {
      backgroundColor: "#333e6e"
  },
  logo: {
    content: "hello"
  },
  spin: {
    animation: "spin 4s linear infinite"
  }

};

class HomeComponent extends Component {
  render() {
    const { classes } = this.props;

    return (
      <AppBar position="static" className={classes.bgBlue}>
        <Toolbar variant="dense">
          <Icon className={classNames("fa fa-xing")}></Icon>
          <Typography variant="title" color="inherit">SCB</Typography>
          <section className={classes.rightToolbar}>
            <IconButton
              onClick={() => {
                window.open("https://facebook.com", "_blank");
              }}
              className={classNames(
                classes.colorWhite,
                "fa fa-facebook-square"
              )}
            />
            <IconButton
              onClick={() => {
                window.open("https://twitter.com", "_blank");
              }}
              className={classNames(classes.colorWhite, "fa fa-twitter-square")}
            />
            <IconButton
              onClick={() => {
                window.open("https://youtube.com", "_blank");
              }}
              className={classNames(classes.colorWhite, "fa fa-youtube-play")}
            />
          </section>
        </Toolbar>
      </AppBar>
    );
  }
}

HomeComponent.propTypes = {
  classes: PropTypes.object.isRequired
};
export default withStyles(styles)(HomeComponent);
