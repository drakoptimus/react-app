import React, { Component } from 'react';
import logo from './logo.svg';
import HeaderComponent from './components/headerComponent';
import SearchComponent from './components/searchComponent';
import './App.css';

class App extends Component {
  render() {
    return (
      <div className="App bgColor">
        <HeaderComponent></HeaderComponent>
        <SearchComponent></SearchComponent> 
      </div>
    );
  }
}

export default App;
